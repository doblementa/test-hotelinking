<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2 text-center mt-5">
                <?php echo app('translator')->get('messages.welcome_message'); ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/Natalia/Sites/Laravel/test-hotelinking/resources/views/welcome.blade.php ENDPATH**/ ?>