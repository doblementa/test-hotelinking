<div class="card mb-2">
  <div class="card-body pb-1">
    <div class="float-left">
      <h5 class="card-title float-left"><?php echo e($code->pivot->code); ?></h5>
      <p>Oferta: <?php echo e(\App\Models\Offer::find($code->pivot->offer_id)->title); ?></p>
    </div>
    <p class="card-text float-right">
        <?php if($code->pivot->changed == null): ?>
            <a href="javascript:void(0);" class="btn btn-primary change-code" code="<?php echo e($code->pivot->code); ?>" offerId="<?php echo e($code->pivot->offer_id); ?>"><?php echo app('translator')->get('messages.changecode'); ?></a>
            <h3 class="text-success text-right change-code-success" code="<?php echo e($code->pivot->code); ?>" style="display:none;"><?php echo app('translator')->get('messages.changedcode'); ?></h3>
        <?php else: ?>
            <p class="text-success text-right"><?php echo app('translator')->get('messages.alreadychangedcodeon', ['date' => \Carbon\Carbon::parse($code->pivot->changed)->format('d/m/Y - H:i')]); ?></p>
        <?php endif; ?>
    </p>
  </div>
</div><?php /**PATH /Users/Natalia/Sites/Laravel/test-hotelinking/resources/views/items/code_details.blade.php ENDPATH**/ ?>