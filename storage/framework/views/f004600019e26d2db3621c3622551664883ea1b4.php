<?php $__env->startSection('content'); ?>
    <h1><?php echo app('translator')->get('messages.availableoffers'); ?></h1>
    <?php if($offers !== null && $offers->count() > 0): ?>
        <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo $__env->make('items.offer_details', ['offer' => $offer, 'generated_offers_count' => Auth::user()->offers()->where('id', $offer->id)->count()], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <?php echo $__env->make('items.error_message', ['title' => __('messages.nooffers_title'), 'message' => __('messages.nooffers_message')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).on('click', '.generate-code', function(e){
            var clickedElement = $(this);
            var offerId = clickedElement.attr('offerId');
            $.ajax({
                type: 'POST',
                url: '<?php echo e(route('ajax.offer.generatecode')); ?>',
                data: {
                    offerId: offerId
                },
                success: function(response) {
                    clickedElement.hide();
                    $('.generated-code[offerId=' + offerId + '] h3').append(response.code);
                    $('.generated-code[offerId=' + offerId + ']').show();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    MyApp.modal.error('<?php echo e(__('messages.general_error_title')); ?>', '<?php echo e(__('messages.general_error_message')); ?>');
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/Natalia/Sites/Laravel/test-hotelinking/resources/views/offers.blade.php ENDPATH**/ ?>