<div class="card mb-2">
  <div class="card-body">
    <h5 class="card-title"><?php echo e($offer->title); ?></h5>
    <p class="card-text"><?php echo e($offer->description); ?></p>
    <?php if($generated_offers_count > 0): ?>
      <p class="font-weight-bold"><?php echo app('translator')->choice('messages.generatedcodes_message', ['count' => $generated_offers_count]); ?></p>
      <a href="javascript:void(0);" class="btn btn-success generate-code" offerId="<?php echo e($offer->id); ?>"><?php echo app('translator')->get('messages.generateanothercode'); ?></a>
    <?php else: ?>
        <a href="javascript:void(0);" class="btn btn-success generate-code" offerId="<?php echo e($offer->id); ?>"><?php echo app('translator')->get('messages.generatecode'); ?></a>
    <?php endif; ?>
    <div class="font-weight-bold generated-code" offerId="<?php echo e($offer->id); ?>" style="display:none;">
      <?php echo app('translator')->choice('messages.generatedcode', 1); ?>
      <h3 class="text-success"></h3>
    </div>
  </div>
</div><?php /**PATH /Users/Natalia/Sites/Laravel/test-hotelinking/resources/views/items/offer_details.blade.php ENDPATH**/ ?>