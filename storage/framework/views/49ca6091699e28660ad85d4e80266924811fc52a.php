<?php $__env->startSection('content'); ?>
    <h1><?php echo app('translator')->get('messages.yourcodes'); ?></h1>
    <?php if($codes !== null && $codes->count() > 0): ?>
    <?php $__currentLoopData = $codes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php echo $__env->make('items.code_details', ['code' => $code], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <?php echo $__env->make('items.error_message', ['title' => __('messages.nocodes_title'), 'message' => __('messages.nocodes_message')], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).on('click', '.change-code', function(e){
            var clickedElement = $(this);
            var offerId = clickedElement.attr('offerId');
            var code = clickedElement.attr('code');
            $.ajax({
                type: 'POST',
                url: '<?php echo e(route('ajax.code.changecode')); ?>',
                data: {
                    offerId: offerId,
                    code: code
                },
                success: function(response) {
                    clickedElement.hide();
                    $('.change-code-success[code=' + code + ']').show();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    MyApp.modal.error('<?php echo e(__('messages.general_error_title')); ?>', '<?php echo e(__('messages.general_error_message')); ?>');
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/Natalia/Sites/Laravel/test-hotelinking/resources/views/codes.blade.php ENDPATH**/ ?>