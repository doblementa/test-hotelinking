require('./bootstrap');

// Ajax token
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    beforeSend: function () {

    },
    complete: function () {
        
    },
    error: function (jqXHR) {
        var data = jqXHR.responseJSON ? jqXHR.responseJSON : JSON.parse(jqXHR.responseText);
        var title = data.title ? data.title : Constants.Translates.message_error_title;
        var message = data.msg ? data.msg : Constants.Translates.message_error_msg;
        MyApp.message.error(title, message);
    }
});


// Modal messages
MyApp = {};
MyApp.modal = {
    error: function (title, message) {
        title = title ? title : Constants.Translates.general_error_title;
        message = message ? message : Constants.Translates.general_error_message;
        $('#alert-modal .modal-title').html(title);
        $('#alert-modal .modal-body p').html(message);
        $('#alert-modal').modal();
    },
    ok: function (title, message) {
        title = title ? title : Constants.Translates.general_ok_title;
        message = message ? message : Constants.Translates.general_ok_message;
        $('#alert-modal .modal-title').html(title);
        $('#alert-modal .modal-body p').html(message);
        $('#alert-modal').modal();
    }
};