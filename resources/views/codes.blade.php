@extends('layouts.app')
@section('content')
    <h1>@lang('messages.yourcodes')</h1>
    @if ($codes !== null && $codes->count() > 0)
    @foreach ($codes as $code)
        @include('items.code_details', ['code' => $code])
    @endforeach
    @else
        @include('items.error_message', ['title' => __('messages.nocodes_title'), 'message' => __('messages.nocodes_message')])
    @endif
@endsection
@section('javascript')
    <script>
        $(document).on('click', '.change-code', function(e){
            var clickedElement = $(this);
            var offerId = clickedElement.attr('offerId');
            var code = clickedElement.attr('code');
            $.ajax({
                type: 'POST',
                url: '{{ route('ajax.code.changecode') }}',
                data: {
                    offerId: offerId,
                    code: code
                },
                success: function(response) {
                    clickedElement.hide();
                    $('.change-code-success[code=' + code + ']').show();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    MyApp.modal.error('{{ __('messages.general_error_title') }}', '{{ __('messages.general_error_message') }}');
                }
            });
        });
    </script>
@endsection