@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2 text-center mt-5">
                @lang('messages.welcome_message')
            </div>
        </div>
    </div>
@endsection