@extends('layouts.app')
@section('content')
    <h1>@lang('messages.availableoffers')</h1>
    @if ($offers !== null && $offers->count() > 0)
        @foreach ($offers as $offer)
            @include('items.offer_details', ['offer' => $offer, 'generated_offers_count' => Auth::user()->offers()->where('id', $offer->id)->count()])
        @endforeach
    @else
        @include('items.error_message', ['title' => __('messages.nooffers_title'), 'message' => __('messages.nooffers_message')])
    @endif
@endsection
@section('javascript')
    <script>
        $(document).on('click', '.generate-code', function(e){
            var clickedElement = $(this);
            var offerId = clickedElement.attr('offerId');
            $.ajax({
                type: 'POST',
                url: '{{ route('ajax.offer.generatecode') }}',
                data: {
                    offerId: offerId
                },
                success: function(response) {
                    clickedElement.hide();
                    $('.generated-code[offerId=' + offerId + '] h3').append(response.code);
                    $('.generated-code[offerId=' + offerId + ']').show();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    MyApp.modal.error('{{ __('messages.general_error_title') }}', '{{ __('messages.general_error_message') }}');
                }
            });
        });
    </script>
@endsection