<div class="card mb-2">
  <div class="card-body">
    <h5 class="card-title">{{ $offer->title }}</h5>
    <p class="card-text">{{ $offer->description }}</p>
    @if ($generated_offers_count > 0)
      <p class="font-weight-bold">@choice('messages.generatedcodes_message', ['count' => $generated_offers_count])</p>
      <a href="javascript:void(0);" class="btn btn-success generate-code" offerId="{{ $offer->id }}">@lang('messages.generateanothercode')</a>
    @else
        <a href="javascript:void(0);" class="btn btn-success generate-code" offerId="{{ $offer->id }}">@lang('messages.generatecode')</a>
    @endif
    <div class="font-weight-bold generated-code" offerId="{{ $offer->id }}" style="display:none;">
      @choice('messages.generatedcode', 1)
      <h3 class="text-success"></h3>
    </div>
  </div>
</div>