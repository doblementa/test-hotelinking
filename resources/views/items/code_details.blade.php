<div class="card mb-2">
  <div class="card-body pb-1">
    <div class="float-left">
      <h5 class="card-title float-left">{{ $code->pivot->code }}</h5>
      <p>Oferta: {{ \App\Models\Offer::find($code->pivot->offer_id)->title }}</p>
    </div>
    <p class="card-text float-right">
        @if ($code->pivot->changed == null)
            <a href="javascript:void(0);" class="btn btn-primary change-code" code="{{ $code->pivot->code }}" offerId="{{ $code->pivot->offer_id }}">@lang('messages.changecode')</a>
            <h3 class="text-success text-right change-code-success" code="{{ $code->pivot->code }}" style="display:none;">@lang('messages.changedcode')</h3>
        @else
            <p class="text-success text-right">@lang('messages.alreadychangedcodeon', ['date' => \Carbon\Carbon::parse($code->pivot->changed)->format('d/m/Y - H:i')])</p>
        @endif
    </p>
  </div>
</div>