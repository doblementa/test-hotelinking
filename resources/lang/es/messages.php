<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'welcome_message' => '
        <h1>Bienvenido a <strong>hotelinking</strong></h1>
            <div>
                <p>Utiliza el menú superior para acceder a las ofertas y códigos promocionales.</p>
                <p>Recuerda que debes estar registrado para poder acceder a ellos.</p>
                <p>¡Suerte!</p>
            </div>
    ',
    'offers' => 'Ofertas',
    'nooffers_title' => 'No hay ofertas que mostrar',
    'nooffers_message' => 'Lo sentimos, en este momento no hay ofertas disponibles.',
    'generatecode' => 'Generar código',
    'generateanothercode' => 'Generar otro código',
    'generatedcode' => 'Código generado|Códigos generados',
    'generatedcodes_message' => 'Tienes :count código generado para esta oferta|Tienes :count códigos generados para esta oferta',
    'availableoffers' => 'Ofertas disponibles',
    'close' => 'Cerrar',
    'mycodes' => 'Mis códigos',
    'yourcodes' => 'Tus códigos',
    'changedcode' => 'Código canjeado',
    'notchangedcode' => 'Código no canjeado',
    'alreadychangedcodeon' => 'Código canjeado el :date',
    'changecode' => 'Canjear código',
    'nocodes_title' => 'No tienes códigos',
    'nocodes_message' => 'Aún no tienes ningún código para canjear',

    // Messages
    'general_error_title' => 'Oops, se ha producido un error',
    'general_error_message' => 'Si el error persite contacta con nosotros',
    'general_ok_title' => 'Operación completada',
    'general_ok_message' => 'La operación se ha completado correctamente',

];
