<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function () { return view('welcome'); });
Route::group(['middleware' => ['auth']], function () {
    Route::get('/offers', 'OfferController@index')->name('offers.index');
    Route::get('/codes', 'CodeController@index')->name('codes.index');
});

Route::group(['prefix' => 'ajax', 'middleware' => ['auth']], function () {
    Route::post('/offer/generate-code', 'AjaxController@offerGenerateCode')->name('ajax.offer.generatecode');
    Route::post('/code/change-code', 'AjaxController@codeChangeCode')->name('ajax.code.changecode');
});