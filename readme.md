1.	Descargar y descomprimir el proyecto en una carpeta del sistema (project_folder)
2.	Desde el ¬terminal acceder a la carpeta del proyecto: cd <project_folder>
3.	Crear la base de datos en local (los datos utilizados habrá que actualizarlos en el proyecto): Nombre de la base de datos: custom_db_name, Codificación: UTF-8, Usuario: custom_db_user, Contraseña: custom_db_pass
4.	Actualizar los siguientes campos en el fichero .env con los datos utilizados al crear la base de datos: DB_DATABASE=custom_db_name, DB_USERNAME=custom_db_user, DB_PASSWORD=custom_db_pass
5.	En el terminal ejecutar los siguientes comandos: php artisan migrate; php artisan db seed;
6.	Abrir el navegador en localhost
