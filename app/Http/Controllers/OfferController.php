<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    // Get all offers
    public function index(Request $request){
        $offers = Offer::all();
        
        return view('offers', [
            'offers' => $offers
        ]);
    }
}
