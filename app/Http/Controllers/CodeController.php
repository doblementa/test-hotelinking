<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CodeController extends Controller
{
    // Get user codes
    public function index(Request $request)
    {
        $user = Auth::user();
        $codes = $user->offers()->get();
        
        return view('codes', [
            'codes' => $codes
        ]);
    }
}
