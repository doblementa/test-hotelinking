<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AjaxController extends Controller
{
    public function offerGenerateCode(Request $request){
        // Get offer id from request
        $offer_id = $request->offerId;

        // Get and check offer
        $offer = Offer::find($offer_id);

        // Create unique code
        $code = str::upper(Str::random(Offer::CODE_LENGTH));

        // Attach offer to logged user
        $user = Auth::user();
        $user->offers()->attach($offer, ['code' => $code]);

        return response()->json([
            'code' => $code
        ]);
    }
    public function codeChangeCode(Request $request){
        // Get data from request
        $offer_id = $request->offerId;
        $code = $request->code;

        // Get and check user offer
        $user = Auth::user();
        $row = $user->offers()->wherePivot('code', $code)->updateExistingPivot($offer_id,  ['changed' => Carbon::now()]);
        
        return response()->json([]);
    }
}
