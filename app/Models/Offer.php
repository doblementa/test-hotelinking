<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    const CODE_LENGTH = 20;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'value',
    ];

    /**
     * Users having the offer
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('code', 'changed');
    }
}
