<?php

use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qty = 10;
        for($i = 0; $i < $qty; $i++){
            $faker = \Faker\Factory::create();
            \DB::table('offers')->insert([
                'title' => $faker->text(30),
                'description' => $faker->realText(),
                'value' => rand(0, 100) + rand(0.01, 0.99),
                //'code' => strtoupper(\Str::random()),
            ]);
        }
    }
}
